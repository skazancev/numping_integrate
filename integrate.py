import json
import logging

import requests


try:
    import urlparse
except ImportError:
    import urllib.parse as urlparse

from asterisk.ami import AMIClient, EventListener

logger = logging.Logger(__name__)


class Config(object):

    def __init__(self, env_file='.env'):
        self.env_file = env_file
        self.parse()

    # Parsing file with environment
    def parse(self):
        f = open(self.env_file)
        for i in f.readlines():
            try:
                name, value = i.replace('\n', '').split('=')
                setattr(self, name, value)
            except ValueError:
                pass

    def get(self, name, default=None):
        if not hasattr(self, name):
            return default

        return getattr(self, name)

    def int(self, name, default):
        value = self.get(name)
        try:
            return int(value)
        except (TypeError, ValueError):
            return default


conf = Config()

# --- Client ami settings ---
AMI_HOST = conf.get('AMI_HOST', '127.0.0.1')
AMI_PORT = conf.int('AMI_PORT', 5038)
AMI_USER = conf.get('AMI_USER')
AMI_PASSWORD = conf.get('AMI_PASSWORD')
# ---

# Contact with numping administration to get token
NUMPING_TOKEN = conf.get('NUMPING_TOKEN')

# Numping url
NUMPING_URL = conf.get('NUMPING_URL', 'https://numping.com/api/v1/integration/events/')

# Receiver field in events. Default is Exten
RECEIVER_FIELD = conf.get('RECEIVER', 'Exten')

# Caller field in events. Default is CallerIDNum
CALLER_FIELD = conf.get('CALLER_FIELD', 'CallerIDNum')


class _EventListener(EventListener):

    # send all events with post request to numping by api
    def on_event(self, event, **kwargs):
        if RECEIVER_FIELD not in event.keys or CALLER_FIELD not in event.keys:
            return

        data = {
            'name': event.name,
            'keys': event.keys,
            'receiver_field': RECEIVER_FIELD,
            'caller_field': CALLER_FIELD
        }
        headers = {
            'Authorization': NUMPING_TOKEN,
            'Content-Type': 'application/json'
        }
        data = json.dumps(data)
        try:
            requests.post(NUMPING_URL, data=data, headers=headers)
        except requests.exceptions.ConnectionError:
            logger.warning('Numping server error')


if __name__ == '__main__':
    import signal
    import sys


    def signal_handler(signal, frame):
        sys.exit(0)


    client = AMIClient(address=AMI_HOST, port=AMI_PORT)
    client.login(username=AMI_USER, secret=AMI_PASSWORD)
    client.add_event_listener(_EventListener())

    signal.signal(signal.SIGINT, signal_handler)
    print('Press Ctrl+C')
    signal.pause()