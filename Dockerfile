FROM python:3.6
ENV PYTHONUNBUFFERED 1

ADD ./ /code/

RUN pip install --no-cache-dir -r /code/requirements.txt

WORKDIR /code
