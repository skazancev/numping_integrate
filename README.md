** Numping integration script **

---

## Installation

copy `env_sample` as `.env`

** requires python 2.7 or 3.3+ **

1. `git clone git@bitbucket.org:skazancev/numping_integrate.git`
2. `pip install -r requirements.txt`
---

## Running

`python integrate.py`
---

## Running with docker

1. `docker-compose build`
2. `docker-compose up -d`
---
